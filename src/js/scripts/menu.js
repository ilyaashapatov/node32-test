/*jslint browser: true*/
/*global $*/
$(document).ready(function () {
    'use strict';
    var $menu = $('[js-menu]');

    if ($menu.length === 0) {
        return;
    }

    var $menuControl = $('[js-menu-control]', $menu);

    $menuControl.on('click', function (event) {
        event.preventDefault();
        $menu.toggleClass('_open');
    });
});