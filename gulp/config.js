var path = require('path');

var src = '../src/',
    dist = 'dist/';

module.exports = {
    css: {
        src: path.join(__dirname, src + 'css/*.sass'),
        prefix: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera > 10', 'Explorer >= 9'],
        target: 'style.css',
        dest: dist + 'styles/'
    },

    images: {
        src: path.join(__dirname, src + 'images/**/*'),
        dest: dist + 'images/'
    },

    spritePng: {
        sprite: true,

        tmpl: './gulp/helpers/sprite-template.handlebars',

        src: path.join(__dirname, src + 'png_sprite/'),
        dest: 'src/images/',

        mixinPath: 'src/css/variables',
        mixinName: 'sprite-mixins.sass',
        mixinPrefix: 'i_',

        imgName: 'sprite.png',
        imgPath: '../images/sprite.png', // этот путь добавляется в шаблон в background-image url

        retina: false,
        retSrc: path.join(__dirname, src + 'png_sprite/*@2x.png'),
        retName: 'sprite@2x.png',
        retPath: '../images/sprite@2x.png' // этот путь добавляется в шаблон в background-image url
    },


    html: {
        src: path.join(__dirname, src + '*.twig'),
        dest: dist
    },

    js: {
        src: path.join(__dirname, src + 'js/app.js'),
        target: 'app.js',
        dest: dist + 'scripts/'
    },

    bower: {
        targetJs: 'vendor.js',
        destJs: dist + 'scripts/',
        targetCss: '_vendor.scss',
        destCss: 'src/css/modules/'
    },

    files: {
        src: path.join(__dirname, src),
        dest: dist
    },

    zip: {
        src: path.join(__dirname, '../dist/**/*'),
        dest: './'
    }
};