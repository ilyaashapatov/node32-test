/* jshint ignore:start */
/* jshint ignore:end */
var config = require('../config').bower,
    gulp = require('gulp'),
    bower = require('main-bower-files'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    gulpIf = require('gulp-if'),
    filter = require('gulp-filter'),
    argv = require('yargs').argv;

// dev mode (если запускать сборку с флагом --dev то статика не будет минифицироваться)
// Пример: $ gulp --dev
gulp.task('bower', function () {
    'use strict';
    var bowerJs = filter(function (file) {
        return file.path.match(/\.(js)$/i);
    }, {restore: true});

    var bowerCss = filter(function (file) {
        return file.path.match(/\.(css)$/i);
    }, {restore: true});

    return gulp.src(bower({checkExistence: true}))
        .pipe(bowerJs)
        .pipe(concat(config.targetJs))
        .pipe(gulpIf(!argv.dev, uglify()))
        .pipe(gulp.dest(config.destJs))
        .pipe(bowerJs.restore)
        .pipe(bowerCss)
        .pipe(concat(config.targetCss))
        .pipe(gulp.dest(config.destCss));
});