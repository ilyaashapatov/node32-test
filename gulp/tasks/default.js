var gulp = require('gulp'),
    run = require('run-sequence');

gulp.task('default', function (done) {
    'use strict';

    run('watch', 'server', done);
});