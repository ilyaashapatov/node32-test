var gulp = require('gulp'),
    browserSync = require('browser-sync');

gulp.task('server', function () {
    'use strict';

    browserSync.init({
        port: 8080,
        notify: false,
        open: false,
        reloadOnRestart: true,
        server: {
            baseDir: 'dist/',
            directory: true
        }
    });
});