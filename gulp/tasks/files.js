var config = require('../config').files,
    gulp = require('gulp');

// fonts
gulp.task('fonts', function () {
    'use strict';
    return gulp.src(config.src + 'fonts/**/*')
        .pipe(gulp.dest(config.dest + 'fonts/'));
});

// concat tasks
// var taskFiles = ['fonts'];

// task: files
gulp.task('files', ['fonts']);

