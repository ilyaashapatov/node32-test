var config = require('../config').js,
    gulp = require('gulp'),

    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,

    gulpIf = require('gulp-if'),
    argv = require('yargs').argv;

var errors = require('../helpers/errors');

// js plugins
var coffee = require('gulp-coffee'),
    concat = require('gulp-concat'),
    requi = require('gulp-requi'),
    uglify = require('gulp-uglify');

// dev mode (если запускать сборку с флагом --dev то статика не будет минифицироваться)
// Пример: $ gulp --dev
gulp.task('js', function () {
    'use strict';

    gulp.src(config.src)
        .pipe(plumber())
        .pipe(requi())
        .pipe(gulpIf(/[.]coffee$/, coffee()))
        .pipe(concat(config.target))
        .pipe(gulpIf(!argv.dev, uglify()))
        .pipe(gulp.dest(config.dest))
        .pipe(reload({stream: true, once: true}));
});