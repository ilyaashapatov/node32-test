// var config = require('../config').proc;
var gulp = require('gulp');

gulp.task('watch', ['build'], function () {
    'use strict';
    gulp.watch(['bower.json', 'package.json'], ['bower']);
    gulp.watch('src/images/**/*', ['images']);
    gulp.watch('src/png_sprite/**/*', ['png-sprite']);
    gulp.watch('src/js/**/*', ['js']);
    gulp.watch(['src/**/*.twig', 'src/**/*.html'], ['html']);
    gulp.watch(['src/fonts/**/*'], ['files']);
    gulp.watch('src/css/**/*', ['css']);
});