var gulp = require('gulp'),
    del = require('del');

gulp.task('clean', function (done) {
    'use strict';

    del(['dist/**'], done);
});