var config = require('../config').spritePng,
    gulp = require('gulp'),
    plumber = require('gulp-plumber');

var spritesmith = require('gulp.spritesmith');

gulp.task('baseSprites', function () {
    'use strict';

    var opts = {
        cssTemplate: config.tmpl,
        imgName: config.imgName,
        imgPath: config.imgPath,
        cssName: config.mixinName,
        padding: 0,
        cssFormat: 'stylus',
        cssVarMap: function (sprite) {
            sprite.prefix = config.mixinPrefix;
            if (config.retina) {
                sprite.retina = true;
                sprite.retinaImgPath = config.retPath;
            } else {
                sprite.retina = false;
            }
        }
    };

    var spriteData = gulp.src([config.src + '*.png', '!' + config.src + '*@*.png']);

    spriteData = spriteData.pipe(
        spritesmith(opts)
    );

    spriteData.css.pipe(plumber()).pipe(gulp.dest(config.mixinPath));
    spriteData.img.pipe(plumber()).pipe(gulp.dest(config.dest));
});

gulp.task('retinaSprites', function () {
    'use strict';

    var opts = {
        imgName: config.retName,
        imgPath: config.retPath,
        padding: 0,
        cssName: '.'
    };

    var spriteData = gulp.src(config.retSrc);

    spriteData = spriteData.pipe(
        spritesmith(opts)
    );

    spriteData.img.pipe(plumber()).pipe(gulp.dest(config.dest));
});

var tasksSprites = ['baseSprites'];

if (config.retina) {
    tasksSprites.push('retinaSprites');
}

gulp.task('png-sprite', tasksSprites);

