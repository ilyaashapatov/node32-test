var config = require('../config').images,
    gulp = require('gulp'),

    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    cached = require('gulp-cached'),

    gulpIf = require('gulp-if'),
    argv = require('yargs').argv;

// images plugins
var imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

// dev mode (если запускать сборку с флагом --dev то статика не будет минифицироваться)
// Пример: $ gulp --dev
gulp.task('images', function () {
    'use strict';

    return gulp.src(config.src)
        .pipe(cached('linting'))
        .pipe(gulpIf(!argv.dev, imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest(config.dest))
        .pipe(reload({stream: true, once: true}));
});