Тестовое задание для компании ESET NOD32

* Gulp
* Twig (html)
* Scss
* JavaScript
* Sprites (png)
* Local server (localhost:8080)

=========================================

### Установка

Устанавливка зависимостей:

```bash
npm i && bower i
```

### Запуск
Существует несколько основных команд при работе со сборщиком:

* `gulp` – Запуск сборки проекта с минификацией js, css и изображений. Запускается событие watch. Доступ из браузера по адресу http://localhost:8080/index.html
* `gulp --dev` – Сборка проекта без минификации и с процессом watch. Доступ из браузера по адресу http://localhost:8080/index.html
* `gulp clean` – Удаление скомпилированных файлов
* `gulp zip` – Упаковка папки dist в архив


## Настрока конфигурации

Все настройки по проекту вынесены в файл `./gulp/config.js`

## Мои контакты

* Email: <mailto:ilyaashapatov@yandex.ru>
* Skype: [ilyaashapatov](skype:ilyaashapatov?chat)